package ru.otus.homework;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.otus.homework.domain.Book;
import ru.otus.homework.domain.Category;
import ru.otus.homework.domain.Publisher;
import ru.otus.homework.repository.BookRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("BookRepositoryTest должен ")
class BookRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private BookRepository bookRepository;

    @Test
    @DisplayName("проверить инициализацию данных")
    public void initializeDatasource() {
        List<Book> bookAll = bookRepository.findAll().stream().collect(Collectors.toList());
        assertAll("Book All assert",
                () -> assertEquals(1, bookAll.size()),
                () -> {
                    Book book = bookAll.get(0);
                    assertAll("First Book assert",
                            () -> assertEquals("ISBN100001-01", book.getIsbn()),
                            () -> assertEquals("OTUS-Spring-01", book.getTitle()),
                            () -> assertEquals(2019, book.getYearPublished()),
                            () -> {
                                List<String> authors = book.getAuthors().stream().map(a -> a.getFirstName() +
                                        " " + a.getLastName()).collect(Collectors.toList());
                                assertAll("Author set assert",
                                        () -> assertEquals(2, authors.size()),
                                        () -> {
                                            assertTrue(authors.contains("OTUS1 GROUP1"), "Author 'OTUS1 GROUP1' is not found");
                                            assertTrue(authors.contains("OTUS2 GROUP2"), "Author 'OTUS2 GROUP2' is not found");
                                        });
                                List<String> categoryNames = book.getCategories().stream().map(Category::getName).collect(Collectors.toList());
                                assertAll("Categories assert",
                                        () -> assertEquals(2, categoryNames.size()),
                                        () -> {
                                            assertTrue(categoryNames.contains("Computers & Technology"));
                                            assertTrue(categoryNames.contains("Programming"));
                                        });
                                Publisher publisher = book.getPublisher();
                                assertAll("Publisher assert",
                                        () -> assertNotNull(publisher),
                                        () -> assertEquals("OTUS Publisher", publisher.getName()));
                            });
                });
    }

    @Test
    @DisplayName("добавить книгу")
    public void insertedBookSuccessfully() {
        Book book = createBook("ISBN-Example01");
        bookRepository.save(book);

        Optional<Book> optBook = bookRepository.findByIsbn("ISBN-Example01");
        assertAll("Found Book assert",
                () -> assertTrue(optBook.isPresent()),
                () -> {
                    Book fBook = optBook.get();
                    assertAll("Created Book assert",
                            () -> assertNotEquals(0, fBook.getId()),
                            () -> assertEquals("Example2", fBook.getTitle()),
                            () -> assertEquals("ISBN-Example01", fBook.getIsbn()),
                            () -> assertEquals(2019, fBook.getYearPublished()));

                    assertTrue(fBook.getAuthors().isEmpty());
                    assertTrue(fBook.getAuthors().isEmpty());
                    assertNull(fBook.getPublisher());
                });
    }

    @Test
    @DisplayName("удалить книгу")
    public void deleteBookSuccessfully() {
        Book book = createBook("ISBN-Example02");
        bookRepository.save(book);

        Optional<Book> fBook = bookRepository.findByIsbn("ISBN-Example02");
        assertTrue(fBook.isPresent());

        bookRepository.delete(fBook.get());
        Optional<Book> deletedBook = bookRepository.findByIsbn("ISBN-Example02");
        assertFalse(deletedBook.isPresent());
    }

    @Test
    @DisplayName("проверить корректную работу при запросе не существующей книги")
    public void findByIdNotExistedBook() {
        assertFalse(bookRepository.findById("None").isPresent());
    }

    private Book createBook(String isbn) {
        Book book = new Book();
        book.setTitle("Example2");
        book.setYearPublished(2019);
        book.setIsbn(isbn);

        return book;
    }

}
