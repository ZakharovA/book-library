package ru.otus.homework.changelog;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.mongodb.client.MongoDatabase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import ru.otus.homework.domain.Author;
import ru.otus.homework.domain.Book;
import ru.otus.homework.domain.Category;
import ru.otus.homework.domain.Publisher;

import java.util.*;
import java.util.stream.Collectors;

@ChangeLog(order = "001")
public class InitMongoDBDataChangeLog {

    private Publisher publisher;
    private final List<Author> authors = new ArrayList<>();
    private final List<Category> categories = new ArrayList<>();

    @ChangeSet(order = "000", id = "dropDB", author = "aszakh", runAlways = true)
    public void dropDb(MongoDatabase database) {
        database.drop();
    }

    @ChangeSet(order = "001", id = "initAuthors", author = "aszakh", runAlways = true)
    public void initAuthors(MongoTemplate mongoTemplate) {
        authors.add(mongoTemplate.save(Author.of("OTUS1", "GROUP1")));        // 1
        authors.add(mongoTemplate.save(Author.of("OTUS2", "GROUP2")));        // 2
        authors.add(mongoTemplate.save(Author.of("AuthorTest", "AuthorTest")));   // 3
    }

    @ChangeSet(order = "002", id = "initCategory", author = "aszakh", runAlways = true)
    public void initCategory(MongoTemplate mongoTemplate) {
        categories.add(mongoTemplate.save(Category.of("Computers & Technology")));    // 1
        categories.add(mongoTemplate.save(Category.of("Programming")));                 // 3
    }

    @ChangeSet(order = "003", id = "initPublisher", author = "aszakh", runAlways = true)
    public void initPublisher(MongoTemplate mongoTemplate) {
        publisher = Publisher.builder().name("OTUS Publisher").
                address("Russia, Otus, Teacher st, 1-1").
                phone("8-800-0000-0-1").build();
        mongoTemplate.save(publisher);                       // 1
    }

    private Set<Category> findCategories(List<String> arrayCategory) {
        return categories.stream().filter(c ->
                arrayCategory.stream().anyMatch(
                        value -> StringUtils.equals(c.getName(), value)
                )).collect(Collectors.toSet());
    }

    private Set<Author> findAuthors(List<String> arrayAuthors) {
        return authors.stream().filter(a ->
                arrayAuthors.stream().anyMatch(
                        value -> StringUtils.equals(a.getLastName(), value)
                )).collect(Collectors.toSet());
    }

    @ChangeSet(order = "004", id = "initBook", author = "aszakh", runAlways = true)
    public void initBook(MongoTemplate mongoTemplate) {
        Book.BookBuilder bookBuilder = Book.builder()
                .title("OTUS-Spring-01")    // 1
                .isbn("ISBN100001-01")
                .summary("The best book")
                .yearPublished(2019)
                .publisher(publisher)
                .categories(new HashSet<>(categories))
                .authors(findAuthors(Arrays.asList("GROUP1", "GROUP2")));
        mongoTemplate.save(bookBuilder.build());
    }
}
