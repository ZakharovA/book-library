package ru.otus.homework;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;

@DataMongoTest
@EnableConfigurationProperties
@ComponentScan({"ru.otus.homework.repository", "ru.otus.homework.config"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class AbstractRepositoryTest {
}
