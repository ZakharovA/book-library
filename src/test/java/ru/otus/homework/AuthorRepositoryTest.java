package ru.otus.homework;

import lombok.val;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import ru.otus.homework.domain.Author;
import ru.otus.homework.repository.AuthorRepository;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("AuthorRepositoryTest должен ")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AuthorRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private AuthorRepository authorRepository;

    @Test
    @Order(1)
    @DisplayName("проверить инициализацию базы сущностями автора")
    public void initDatabaseByAuthors() {
        List<Author> authors = authorRepository.findAll();

        assertAll("Authors assert",
                () -> assertFalse(authors.isEmpty()),
                () -> assertEquals(3, authors.size()),
                () -> {
                    List<String> strAuthors = authors.stream().map(a -> a.getFirstName() + " " + a.getLastName()).collect(Collectors.toList());

                    assertAll("Contain authors assert",
                            () -> assertTrue(strAuthors.contains("OTUS1 GROUP1")),
                            () -> assertTrue(strAuthors.contains("OTUS2 GROUP2")),
                            () -> assertTrue(strAuthors.contains("AuthorTest AuthorTest")));
                });
    }

    @Test
    @Order(2)
    @DisplayName("удалить автора")
    public void removeAuthor() {
        val all = authorRepository.findAll();
        assertThat(all).isNotNull().hasSize(3);

        val author = authorRepository.findByLastName("AuthorTest").get();
        val countBooks = authorRepository.countBooks(author.getId());
        assertThat(countBooks).isZero();

        authorRepository.deleteById(author.getId());

        val updAll = authorRepository.findAll();
        assertThat(updAll).isNotNull().hasSize(2);
    }

    @Test
    @Order(3)
    @DisplayName("добавить автора")
    public void insertAuthor() {
        val response = authorRepository.save(new Author("NEW", "Author"));
        assertThat(response).isNotNull();

        Author author = authorRepository.findByLastName("Author").get();
        assertThat(author).isNotNull().isEqualTo(response);
    }
}
