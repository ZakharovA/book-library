package ru.otus.homework.controller;

import java.util.Arrays;
import java.util.Optional;

public enum EntityType {

    BOOK("book"),
    AUTHOR("author"),
    CATEGORY("category"),
    PUBLISHER("publisher");

    private final String title;

    EntityType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public static EntityType get(String title) {
        Optional<EntityType> optEntityType = Arrays.stream(EntityType.values()).filter(et -> et.getTitle().equals(title)).findFirst();
        if (optEntityType.isPresent()) {
            return optEntityType.get();
        }

        throw new IllegalArgumentException("Entity Type '" + title + "' is not found");
    }
}
