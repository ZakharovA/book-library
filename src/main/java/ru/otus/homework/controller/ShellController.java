package ru.otus.homework.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.otus.homework.service.operation.*;

@ShellComponent
@RequiredArgsConstructor
public class ShellController {

    private final CommandFactory commandFactory;

    @ShellMethod("View info about Entity")
    public String info(
            @ShellOption(help = "Entity Type: book, author, category, publisher") String entityType,
            @ShellOption(defaultValue = "list", help = "Key operation, see help for required Entity") String operation,
            @ShellOption(defaultValue = "", help = "Search value") String searchValue
    ) {
        try {
            InfoViewer infoViewer = commandFactory.get(entityType).getInfoViewer();
            return infoViewer.info(operation, searchValue);
        } catch (IllegalArgumentException ex) {
            return ex.getMessage();
        } catch (EmptyResultDataAccessException ex) {
            return "The " + entityType + " is not found";
        }
    }

    @ShellMethod("Delete Entity by Key")
    public String delete(
            @ShellOption(help = "Entity Type: book, author, category, publisher") String entityType,
            @ShellOption(help = "Title or Name of Entity") String name
    ) {
        try {
            DeleteEntity deleteEntity = commandFactory.get(entityType).getDeleteEntity();
            return deleteEntity.delete(name);
        } catch (IllegalArgumentException ex) {
            return ex.getMessage();
        }
    }

    @ShellMethod("Create Entity")
    public String create(
            @ShellOption(help = "Entity Type: book, author, category, publisher") String entityType,
            @ShellOption(defaultValue = "") String value1,
            @ShellOption(defaultValue = "") String value2,
            @ShellOption(defaultValue = "") String value3
    ) {
        try {
            EntityBuilder entityBuilder = commandFactory.get(entityType).getEntityBuilder();
            return entityBuilder.create(value1, value2, value3);
        } catch (IllegalArgumentException ex) {
            return ex.getMessage();
        }
    }

    @ShellMethod("Bind Entity to Book")
    public String add(
            @ShellOption(help = "Name field of book: summary, author, category, publisher") String fieldName,
            @ShellOption(help = "This is Title of the book") String titleBook,
            @ShellOption(help = "This is text of summary field or ID of entity for other cases", defaultValue = "0") String value
    ) {
        try {
            EntityBinder entityBinder = commandFactory.get(EntityType.BOOK.getTitle()).getEntityBinder(fieldName);
            return entityBinder.bind(titleBook, value);
        } catch (IllegalArgumentException ex) {
            return ex.getMessage();
        }
    }
}

