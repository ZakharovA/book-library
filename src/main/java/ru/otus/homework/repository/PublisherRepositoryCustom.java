package ru.otus.homework.repository;

public interface PublisherRepositoryCustom {

    int countBooks(String publisherId);
}
