package ru.otus.homework.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.otus.homework.domain.Category;

import java.util.Optional;

public interface CategoryRepository
        extends MongoRepository<Category, String>, CategoryRepositoryCustom {

    Optional<Category> findByName(String name);
}
