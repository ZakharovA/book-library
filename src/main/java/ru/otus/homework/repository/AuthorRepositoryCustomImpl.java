package ru.otus.homework.repository;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import ru.otus.homework.domain.Book;

import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@RequiredArgsConstructor
public class AuthorRepositoryCustomImpl implements AuthorRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    @Data
    private class BookAggregation{
        private String id;
    }

    @Override
    public int countBooks(String authorId) {
        Aggregation authors = newAggregation(unwind("authors"),
                match(Criteria.where("authors.id").is(authorId)),
                project().and("id").as("id"));

        List<BookAggregation> mappedResults = mongoTemplate.aggregate(
                authors, Book.class, BookAggregation.class).getMappedResults();
        return mappedResults.size();
    }
}
