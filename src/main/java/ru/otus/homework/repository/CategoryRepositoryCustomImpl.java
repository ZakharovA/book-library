package ru.otus.homework.repository;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import ru.otus.homework.domain.Book;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@RequiredArgsConstructor
public class CategoryRepositoryCustomImpl implements CategoryRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    @Data
    private class BookAggregation {
        private String id;
    }

    @Override
    public int countBooks(String categoryId) {
        val authors = newAggregation(unwind("categories"),
                match(Criteria.where("categories.id").is(categoryId)),
                project().and("id").as("as"));

        return mongoTemplate.aggregate(authors, Book.class, BookAggregation.class).getMappedResults().size();
    }
}
