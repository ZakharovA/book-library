package ru.otus.homework.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.otus.homework.domain.Publisher;

import java.util.Optional;

public interface PublisherRepository
        extends MongoRepository<Publisher, String>, PublisherRepositoryCustom {

    Optional<Publisher> findByName(String name);
}
