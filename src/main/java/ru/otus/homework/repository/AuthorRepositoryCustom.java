package ru.otus.homework.repository;

public interface AuthorRepositoryCustom {

    int countBooks(String authorId);
}
