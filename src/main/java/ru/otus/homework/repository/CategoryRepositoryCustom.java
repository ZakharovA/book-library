package ru.otus.homework.repository;

public interface CategoryRepositoryCustom {

    int countBooks(String categoryId);
}
