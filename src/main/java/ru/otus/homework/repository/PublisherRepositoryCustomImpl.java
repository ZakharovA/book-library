package ru.otus.homework.repository;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import ru.otus.homework.domain.Book;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@RequiredArgsConstructor
public class PublisherRepositoryCustomImpl implements PublisherRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    @Data
    private class BookAggregation {
        private String id;
    }

    @Override
    public int countBooks(String publisherId) {
        val agr = newAggregation(unwind("publisher"),
                match(Criteria.where("publisher.id").is(publisherId)),
                project().and("id").as("id"));

        return mongoTemplate.aggregate(agr, Book.class, BookAggregation.class)
                .getMappedResults().size();
    }
}
