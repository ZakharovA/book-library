package ru.otus.homework.changelog;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.mongodb.client.MongoDatabase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import ru.otus.homework.domain.Author;
import ru.otus.homework.domain.Book;
import ru.otus.homework.domain.Category;
import ru.otus.homework.domain.Publisher;

import java.util.*;
import java.util.stream.Collectors;

@ChangeLog(order = "001")
public class InitMongoDBDataChangeLog {

    private final Map<String, Publisher> publishers = new HashMap<>();
    private final List<Author> authors = new ArrayList<>();
    private final List<Category> categories = new ArrayList<>();

    @ChangeSet(order = "000", id = "dropDB", author = "aszakh", runAlways = true)
    public void dropDb(MongoDatabase database) {
        database.drop();
    }

    @ChangeSet(order = "001", id = "initAuthors", author = "aszakh", runAlways = true)
    public void initAuthors(MongoTemplate mongoTemplate) {
        authors.add(mongoTemplate.save(Author.of("OTUS1", "GROUP")));
        authors.add(mongoTemplate.save(Author.of("OTUS2", "GROUP")));
        authors.add(mongoTemplate.save(Author.of("Sun", "Microsystems")));
        authors.add(mongoTemplate.save(Author.of("Oracle", "Company")));
        authors.add(mongoTemplate.save(Author.of("Santa", "Claus")));
        authors.add(mongoTemplate.save(Author.of("Micky", "Mouse")));
        authors.add(mongoTemplate.save(Author.of("Dog", "Doges")));
    }

    @ChangeSet(order = "002", id = "initCategory", author = "aszakh", runAlways = true)
    public void initCategory(MongoTemplate mongoTemplate) {
        categories.add(mongoTemplate.save(Category.of("Computers and Technology")));
        categories.add(mongoTemplate.save(Category.of("Adventures")));
        categories.add(mongoTemplate.save(Category.of("Programming")));
        categories.add(mongoTemplate.save(Category.of("Kids")));
        categories.add(mongoTemplate.save(Category.of("Hobbies")));
    }

    @ChangeSet(order = "003", id = "initPublisher", author = "aszakh", runAlways = true)
    public void initPublisher(MongoTemplate mongoTemplate) {
        publishers.put("OTUS Publisher", mongoTemplate.save(
                Publisher.builder()
                        .name("OTUS Publisher")
                        .address("Russia, Otus, Teacher st, 1-1")
                        .phone("8-800-0000-0-1")
                        .build()
        ));
        publishers.put("Piter Publisher", mongoTemplate.save(
                Publisher.builder()
                        .name("Piter Publisher")
                        .address("Russia, Saint-Petersburg, Street")
                        .phone("8-800-0000-0-2")
                        .build()
        ));
        publishers.put("Santa claus", mongoTemplate.save(
                Publisher.builder()
                        .name("Santa claus")
                        .address("Russia, Moscow, Scolcovo")
                        .build()
        ));
    }

    private Set<Category> findCategories(List<String> arrayCategory) {
        return categories.stream().filter(c ->
                arrayCategory.stream().anyMatch(
                        value -> StringUtils.equals(c.getName(), value)
                )).collect(Collectors.toSet());
    }

    private Set<Author> findAuthors(List<String> arrayAuthors) {
        return authors.stream().filter(a ->
                arrayAuthors.stream().anyMatch(
                        value -> StringUtils.equals(a.getLastName(), value)
                )).collect(Collectors.toSet());
    }

    @ChangeSet(order = "004", id = "initBook", author = "aszakh", runAlways = true)
    public void initBook(MongoTemplate mongoTemplate) {
        Book book = Book.builder()
                .title("OTUS-Spring-01")
                .isbn("ISBN100001-01")
                .summary("The best book")
                .yearPublished(2019)
                .publisher(publishers.get("OTUS Publisher"))
                .categories(
                        findCategories(
                                Arrays.asList("Computers and Technology", "Programming")
                        ))
                .authors(findAuthors(Collections.singletonList("GROUP")))
                .build();
        mongoTemplate.save(book);

        book = Book.builder()
                .title("Java Thinking")
                .isbn("ISBN100001-01")
                .yearPublished(2010)
                .publisher(publishers.get("Piter Publisher"))
                .categories(
                        findCategories(
                                Arrays.asList("Computers and Technology", "Programming")
                        ))
                .authors(findAuthors(Arrays.asList("Microsystems", "Company")))
                .build();
        mongoTemplate.save(book);

        book = Book.builder()
                .title("Little mouse")
                .isbn("ISBN100001-03")
                .summary("History about little mouse")
                .yearPublished(2016)
                .publisher(publishers.get("Santa claus"))
                .categories(findCategories(Arrays.asList("Adventures", "Kids")))
                .authors(findAuthors(Arrays.asList("Claus", "Mouse")))
                .build();
        mongoTemplate.save(book);

        book = Book.builder()
                .title("Amazing Grace: A Dog's Tale")
                .isbn("ISBN-0761129758")
                .yearPublished(2016)
                .publisher(publishers.get("Santa claus"))
                .categories(findCategories(Collections.singletonList("Hobbies")))
                .authors(findAuthors(Collections.singletonList("Doges")))
                .build();
        mongoTemplate.save(book);

        book = Book.builder()
                .title("TITLE")
                .isbn("ISBN-0000000001")
                .yearPublished(2019)
                .build();
        mongoTemplate.save(book);
    }
}
