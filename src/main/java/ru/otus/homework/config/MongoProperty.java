package ru.otus.homework.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Validated
@Component
@ConfigurationProperties(prefix = "spring.data.mongodb")
public class MongoProperty {

    @Min(1000)
    private int port;

    @NotNull
    @NotBlank
    private String database;

    @NotNull
    @NotBlank
    private String uri;
}
