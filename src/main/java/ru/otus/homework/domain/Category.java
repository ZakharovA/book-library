package ru.otus.homework.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Category {

    @Id
    public String id;
    public String name;

    public Category(String name) {
        this.name = name;
    }

    public static Category of(String name) {
        return new Category(name);
    }

}
