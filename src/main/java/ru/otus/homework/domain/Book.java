package ru.otus.homework.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collections;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document
@Builder
public class Book {

    @Id
    private String id;
    private String isbn;
    private String title;
    private String summary;
    private int yearPublished;

    @DBRef
    private Set<Author> authors = Collections.emptySet();

    @DBRef
    private Publisher publisher;

    @DBRef
    private Set<Category> categories = Collections.emptySet();
}
