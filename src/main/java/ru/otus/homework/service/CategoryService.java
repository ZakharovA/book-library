package ru.otus.homework.service;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;
import ru.otus.homework.domain.Category;
import ru.otus.homework.repository.CategoryRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository repository;

    public List<Category> getAll() {
        return repository.findAll();
    }

    public Optional<Category> getById(String id) {
        return repository.findById(id);
    }

    public Optional<Category> getByName(String name) {
        return repository.findByName(name);
    }

    public String create(String name) {
        Category category = new Category();
        category.setName(name);

        repository.save(category);
        return "success";
    }

    public String delete(String name) {
        val optCategory = repository.findByName(name);
        return optCategory.map(c -> {
            if (repository.countBooks(c.getId()) != 0) {
                return "It is impossible delete the category";
            }

            repository.deleteById(c.getId());
            return "success";
        }).orElse("Category is not found");
    }
}
