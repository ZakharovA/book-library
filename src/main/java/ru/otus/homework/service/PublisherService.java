package ru.otus.homework.service;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import ru.otus.homework.domain.Publisher;
import ru.otus.homework.repository.PublisherRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PublisherService {

    private final PublisherRepository repository;

    public List<Publisher> getAll() {
        return repository.findAll();
    }

    public Optional<Publisher> getById(String id) {
        return repository.findById(id);
    }

    public Optional<Publisher> getByName(String name) {
        return repository.findByName(name);
    }

    public String create(String name, String address, String phone) {
        Publisher publisher = new Publisher();
        publisher.setName(name);
        if (StringUtils.isNotEmpty(address)) {
            publisher.setAddress(address);
        }
        if (StringUtils.isNotEmpty(phone)) {
            publisher.setPhone(phone);
        }

        repository.save(publisher);
        return "success";
    }

    public String delete(String name) {
        val optPublisher = repository.findByName(name);
        return optPublisher.map(p -> {
            if (repository.countBooks(p.getId()) != 0) {
                return "It is impossible delete the publisher";
            }

            repository.deleteById(p.getId());
            return "success";
        }).orElse("Publisher is not found");
    }
}
