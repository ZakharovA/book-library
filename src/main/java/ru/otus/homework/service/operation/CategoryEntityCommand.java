package ru.otus.homework.service.operation;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import ru.otus.homework.domain.Category;
import ru.otus.homework.service.CategoryService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("CategoryEntityCommand")
@RequiredArgsConstructor
public class CategoryEntityCommand implements EntityCommand {

    private final CategoryService categoryService;

    @Override
    public InfoViewer getInfoViewer() {
        return (String operation, String searchValue) -> {
            List<Category> categories;

            switch (operation) {
                case "-l":
                case "list":
                    categories = categoryService.getAll();
                    break;
                case "-id":
                case "find-by-id":
                    try {
                        Optional<Category> iOptCategory = categoryService.getById(searchValue);
                        categories = iOptCategory.map(Collections::singletonList).orElse(Collections.emptyList());
                    } catch (NumberFormatException ex) {
                        return "The search value is unspecified";
                    }
                    break;
                case "-name":
                case "find-by-name":
                    Optional<Category> optCategory = categoryService.getByName(searchValue);
                    categories = optCategory.map(Collections::singletonList).orElse(Collections.emptyList());
                    break;
                case "-h":
                case "help":
                    return "Help: info category [operation] [search value]\n" +
                            "\t-l, list\t\t\t\tshow all categories\n" +
                            "\t-id, find-by-id\t\t\tfind category by ID value\n" +
                            "\t-name, find-by-name\t\tfind category by Name";
                default:
                    return "Unknown operation";
            }
            if (categories.isEmpty()) {
                return "There is no category(s)";
            }

            return categories.stream().map(this::info).collect(Collectors.joining("\n"));
        };
    }

    private String info(Category category) {
        return "-ID: " + category.getId() + ";\tName: " + category.getName();
    }

    @Override
    public DeleteEntity getDeleteEntity() {
        return categoryService::delete;
    }

    @Override
    public EntityBuilder getEntityBuilder() {
        return (String name, String missing1, String missing2) -> {
            if (StringUtils.equals("-h", name) || StringUtils.equals("help", name)) {
                return "Help: create value1\n" +
                        "\tvalue1 - the name of the category";
            }

            return categoryService.create(name);
        };
    }

    @Override
    public EntityBinder getEntityBinder(String fieldName) {
        return (String titleBook, String value) -> "Unsupported command";
    }
}
