package ru.otus.homework.service.operation;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import ru.otus.homework.domain.Publisher;
import ru.otus.homework.service.PublisherService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("PublisherEntityCommand")
@RequiredArgsConstructor
public class PublisherEntityCommand implements EntityCommand {

    private final PublisherService publisherService;

    @Override
    public InfoViewer getInfoViewer() {
        return (String operation, String searchValue) -> {
            List<Publisher> publishers;
            switch (operation) {
                case "-l":
                case "list":
                    publishers = publisherService.getAll();
                    break;
                case "-id":
                case "find-by-id":
                    try {
                        Optional<Publisher> iOptPublisher = publisherService.getById(searchValue);
                        publishers = iOptPublisher.map(Collections::singletonList).orElse(Collections.emptyList());
                    } catch (NumberFormatException ex) {
                        return "The search value is unspecified";
                    }
                    break;
                case "-name":
                case "find-by-name":
                    Optional<Publisher> optPublisher = publisherService.getByName(searchValue);
                    publishers = optPublisher.map(Collections::singletonList).orElse(Collections.emptyList());
                    break;
                case "-h":
                case "help":
                    return "Help: info publisher [operation] [search value]\n" +
                            "\t-l, list\t\t\t\tshow all publishers\n" +
                            "\t-id, find-by-id\t\t\tfind publisher by ID value\n" +
                            "\t-name, find-by-name\t\tfind publisher by Name";
                default:
                    return "Unknown operation";
            }
            if (publishers.isEmpty()) {
                return "There is no publisher(s)";
            }
            return publishers.stream().map(this::info).collect(Collectors.joining("\n"));
        };
    }

    private String info(Publisher publisher) {
        StringBuilder info = new StringBuilder();

        info.append("- ID: ").append(publisher.getId()).
                append(";\tName: ").append(publisher.getName());
        if (publisher.getAddress() != null && !publisher.getAddress().isEmpty()) {
            info.append(";\tAddress: ").append(publisher.getAddress());
        }
        if (publisher.getPhone() != null && !publisher.getPhone().isEmpty()) {
            info.append(";\tPhone: ").append(publisher.getPhone());
        }

        return info.toString();
    }

    @Override
    public DeleteEntity getDeleteEntity() {
        return publisherService::delete;
    }

    @Override
    public EntityBuilder getEntityBuilder() {
        return (String name, String address, String phone) -> {
            if (StringUtils.equals("-h", name) || StringUtils.equals("help", name)) {
                return "Help: create publisher value1 [value2] [value3]\n" +
                        "\tvalue1 - the Name of the Publisher\n" +
                        "\tvalue2 - the Address of the Publisher. Field is not mandatory\n" +
                        "\tvalue3 - the Phone of the Publisher. Field is not mandatory";
            }

            return publisherService.create(name, address, phone);
        };
    }

    @Override
    public EntityBinder getEntityBinder(String fieldName) {
        return (String titleBook, String value) -> "Unsupported command";
    }
}
