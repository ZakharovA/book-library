package ru.otus.homework.service.operation;

@FunctionalInterface
public interface EntityBuilder {

    String create(String value1, String value2, String value3);
}
