package ru.otus.homework.service.operation;

@FunctionalInterface
public interface DeleteEntity {

    String delete(String name);
}
