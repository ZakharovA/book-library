package ru.otus.homework.service.operation;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.otus.homework.controller.EntityType;

@Service
@RequiredArgsConstructor
public class CommandFactory {

    @Qualifier("BookEntityCommand")
    @NonNull
    private final EntityCommand bookEntityCommand;

    @NonNull
    @Qualifier("AuthorEntityCommand")
    private final EntityCommand authorEntityCommand;

    @Qualifier("CategoryEntityCommand")
    @NonNull
    private final EntityCommand categoryEntityCommand;

    @Qualifier("PublisherEntityCommand")
    @NonNull
    private final EntityCommand publisherEntityCommand;

    public EntityCommand get(String entityType) {
        switch (EntityType.get(entityType)) {
            case BOOK:
                return bookEntityCommand;
            case AUTHOR:
                return authorEntityCommand;
            case CATEGORY:
                return categoryEntityCommand;
            case PUBLISHER:
                return publisherEntityCommand;
        }

        throw new IllegalArgumentException("EntityCommand is not found");
    }
}
