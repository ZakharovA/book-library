package ru.otus.homework.service.operation;

@FunctionalInterface
public interface EntityBinder {

    String bind(String entityKey, String value);
}
