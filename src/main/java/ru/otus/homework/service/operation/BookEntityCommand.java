package ru.otus.homework.service.operation;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import ru.otus.homework.controller.EntityType;
import ru.otus.homework.domain.Author;
import ru.otus.homework.domain.Book;
import ru.otus.homework.domain.Category;
import ru.otus.homework.domain.Publisher;
import ru.otus.homework.service.BookService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service("BookEntityCommand")
@RequiredArgsConstructor
public class BookEntityCommand implements EntityCommand {

    private final BookService bookService;

    @Override
    public InfoViewer getInfoViewer() {
        return (String operation, String searchValue) -> {
            List<Book> result;
            switch (operation) {
                case "-l":
                case "list":
                    result = bookService.getAllBook();
                    break;
                case "-isbn":
                case "find-by-isbn":
                    if (StringUtils.isEmpty(searchValue)) {
                        return "The search value is unspecified";
                    }
                    Optional<Book> iOptBook = bookService.getBookByIsbn(searchValue);
                    result = iOptBook.map(Collections::singletonList).orElse(Collections.emptyList());
                    break;
                case "-id":
                case "find-by-id":
                    Optional<Book> optBook = bookService.getBookById(searchValue);
                    result = optBook.map(Collections::singletonList).orElse(Collections.emptyList());
                    break;
                case "-t":
                case "find-by-title":
                    Optional<Book> optTitleBook = bookService.getBookByTitle(searchValue);
                    result = optTitleBook.map(Collections::singletonList).orElse(Collections.emptyList());
                    break;
                case "-h":
                case "help":
                    return "Help: info book [operation] [search value]\n" +
                            "\t-l, list\t\t\t\tshow all books\n" +
                            "\t-isbn, find-by-isbn\t\tfind book by ISBN\n" +
                            "\t-t, find-by-title\t\tfind book by Title\n" +
                            "\t-id, find-by-id\t\t\tfind book by ID value";
                default:
                    return "Unknown operation";
            }

            if (result.isEmpty()) {
                return "Book(s) is not found";
            }
            return result.stream().map(this::getBookInfo).collect(Collectors.joining("\n"));
        };
    }

    private String getBookInfo(Book book) {
        StringBuilder bookString = new StringBuilder();
        bookString.append("- ID: ").append(book.getId()).
                append(";\tTitle: \"").append(book.getTitle()).append("\"").
                append(";\tISBN: ").append(book.getIsbn()).
                append(";\tYear Publisher: ").append(book.getYearPublished());

        Publisher publisher = book.getPublisher();
        if (publisher != null) {
            bookString.append("\n\tPublisher:").
                    append("\n\t\tName: ").append(publisher.getName());
            if (publisher.getAddress() != null) {
                bookString.append("\tAddress: ").append(publisher.getAddress());
            }
            if (publisher.getPhone() != null) {
                bookString.append("\tPhone: ").append(publisher.getPhone());
            }
        }

        Set<Category> categories = book.getCategories();
        if (categories != null && !categories.isEmpty()) {
            bookString.append("\n\tCategor").append(categories.size() == 1 ? "y" : "ies").
                    append(": ");
            bookString.append(categories.stream().map(Category::getName).collect(Collectors.joining("; ")));
        }

        Set<Author> authors = book.getAuthors();
        if (authors != null && !authors.isEmpty()) {
            bookString.append("\n\tAuthor").append(authors.size() == 1 ? "" : "s").
                    append(": ");
            bookString.append(authors.stream().map(author -> author.getLastName() + " " + author.getFirstName()).collect(Collectors.joining("; ")));
        }

        if (book.getSummary() != null && !org.springframework.util.StringUtils.isEmpty(book.getSummary())) {
            bookString.append("\n\tSummary: ").append(book.getSummary());
        }

        return bookString.toString();
    }

    @Override
    public DeleteEntity getDeleteEntity() {
        return bookService::delete;
    }

    @Override
    public EntityBuilder getEntityBuilder() {
        return (String title, String isbn, String yearPublisher) -> {
            if (StringUtils.equals("-h", title) || StringUtils.equals("help", title)) {
                return "Help: create book value1 value2 value3\n" +
                        "\tvalue1 - the Title of the book\n" +
                        "\tvalue2 - the ISBN of the book\n" +
                        "\tvalue3 - the Year Publisher of the book";
            }

            return bookService.create(title, isbn, Integer.valueOf(yearPublisher));
        };
    }

    @Override
    public EntityBinder getEntityBinder(String fieldName) {
        if ("summary".equals(fieldName)) {
            return bookService::addSummary;
        }

        EntityType entityType = EntityType.get(fieldName);
        switch (entityType) {
            case AUTHOR:
                return bookService::addAuthor;
            case CATEGORY:
                return bookService::addCategory;
            case PUBLISHER:
                return bookService::addPublisher;
        }

        throw new IllegalArgumentException("Field name is not found");
    }
}
