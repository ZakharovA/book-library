package ru.otus.homework.service.operation;

public interface EntityCommand {

    InfoViewer getInfoViewer();
    DeleteEntity getDeleteEntity();
    EntityBuilder getEntityBuilder();
    EntityBinder getEntityBinder(String fieldName);
}
