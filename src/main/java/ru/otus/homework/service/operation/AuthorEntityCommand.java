package ru.otus.homework.service.operation;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import ru.otus.homework.domain.Author;
import ru.otus.homework.service.AuthorService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("AuthorEntityCommand")
@RequiredArgsConstructor
public class AuthorEntityCommand implements EntityCommand {

    private final AuthorService authorService;

    @Override
    public InfoViewer getInfoViewer() {
        return (String operation, String searchValue) -> {
            List<Author> authorList;

            switch (operation) {
                case "-l":
                case "list":
                    authorList = authorService.getAll();
                    break;
                case "-id":
                case "find-by-id":
                    try {
                        Optional<Author> iOptAuthor = authorService.getById(searchValue);
                        authorList = iOptAuthor.map(Collections::singletonList).orElse(Collections.emptyList());
                    } catch (NumberFormatException ex) {
                        return "The search value is unspecified";
                    }
                    break;
                case "-ln":
                case "find-by-last-name":
                    Optional<Author> optAuthor = authorService.getByLastName(searchValue);
                    authorList = optAuthor.map(Collections::singletonList).orElse(Collections.emptyList());
                    break;
                case "-h":
                case "help":
                    return "Help: info author [operation] [search value]\n" +
                            "\t-l, list\t\t\t\t\tshow all authors\n" +
                            "\t-id, find-by-id\t\t\t\tfind author by ID value\n" +
                            "\t-ln, find-by-last-name\t\tfind author by Last Name";
                default:
                    return "Unknown operation";
            }
            if (authorList.isEmpty()) {
                return "There is no author(s)";
            }
            return authorList.stream().map(this::info).collect(Collectors.joining("\n"));
        };
    }

    private String info(Author author) {
        return "- ID: " + author.getId() + ";\tLast name: " + author.getLastName() + ";\tFirst name: " + author.getFirstName();
    }

    @Override
    public DeleteEntity getDeleteEntity() {
        return authorService::delete;
    }

    @Override
    public EntityBuilder getEntityBuilder() {
        return (String firstName, String lastName, String missing) -> {
            if (StringUtils.equals("-h", firstName) || StringUtils.equals("help", firstName)) {
                return "Help: create author value1 value2\n" +
                        "\tvalue1 - the First Name of author\n" +
                        "\tvalue2 - the Last Name of author";
            }

            return authorService.create(firstName, lastName);
        };
    }

    @Override
    public EntityBinder getEntityBinder(String fieldName) {
        return (String titleBook, String value) -> "Unsupported command";
    }
}
