package ru.otus.homework.service.operation;

@FunctionalInterface
public interface InfoViewer {

    String info(String operation, String search);
}
