package ru.otus.homework.service;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;
import ru.otus.homework.domain.Author;
import ru.otus.homework.repository.AuthorRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository repository;

    public List<Author> getAll() {
        return repository.findAll();
    }

    public Optional<Author> getById(String id) {
        return repository.findById(id);
    }

    public Optional<Author> getByLastName(String lastName) {
        return repository.findByLastName(lastName);
    }

    public String create(String firstName, String lastName) {
        Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);

        repository.save(author);
        return "success";
    }

    public String delete(String lastName) {
        val optAuthor = repository.findByLastName(lastName);
        return optAuthor.map(a -> {
            if (repository.countBooks(a.getId()) != 0) {
                return "It is impossible delete the author";
            }

            repository.deleteById(a.getId());
            return "success";
        }).orElse("Author is not found");
    }
}
