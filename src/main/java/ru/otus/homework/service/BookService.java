package ru.otus.homework.service;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;
import ru.otus.homework.domain.Author;
import ru.otus.homework.domain.Book;
import ru.otus.homework.domain.Category;
import ru.otus.homework.repository.AuthorRepository;
import ru.otus.homework.repository.BookRepository;
import ru.otus.homework.repository.CategoryRepository;
import ru.otus.homework.repository.PublisherRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;
    private final PublisherRepository publisherRepository;
    private final AuthorRepository authorRepository;
    private final CategoryRepository categoryRepository;

    public List<Book> getAllBook() {
        return bookRepository.findAll();
    }

    public Optional<Book> getBookByIsbn(String isbn) {
        return bookRepository.findByIsbn(isbn);
    }

    public Optional<Book> getBookById(String id) {
        return bookRepository.findById(id);
    }

    public Optional<Book> getBookByTitle(String title) {
        return bookRepository.findByTitle(title);
    }

    public String delete(String title) {
        val book = bookRepository.findByTitle(title);
        return book.map(b -> {
            bookRepository.deleteById(book.get().getId());
            return "Success";
        }).orElse("Book is not found");
    }

    public String create(String title, String isbn, int yearPublished) {
        Book book = new Book();
        book.setTitle(title);
        book.setIsbn(isbn);
        book.setYearPublished(yearPublished);

        bookRepository.save(book);
        return "Success";
    }

    public String addSummary(String title, String summary) {
        val byTitle = bookRepository.findByTitle(title);

        return byTitle.map(book -> {
            book.setSummary(summary);
            bookRepository.save(book);
            return "success";
        }).orElse("Book is not found");
    }

    public String addPublisher(String titleBook, String namePublisher) {
        val optPublisher = publisherRepository.findByName(namePublisher);

        return optPublisher.map(publisher -> {
            val optBook = bookRepository.findByTitle(titleBook);
            return optBook.map(book -> {
                book.setPublisher(publisher);
                bookRepository.save(book);
                return "success";
            }).orElse("Book is not found");
        }).orElse("Publisher is not found");
    }

    public String addAuthor(String titleBook, String lastNameAuthor) {
        val optAuthor = authorRepository.findByLastName(lastNameAuthor);

        return optAuthor.map(author -> {
            val optBook = bookRepository.findByTitle(titleBook);

            return optBook.map(book -> {
                Set<Author> authors = book.getAuthors();
                if (authors == null) {
                    authors = new HashSet<>();
                    book.setAuthors(authors);
                }
                authors.add(author);
                bookRepository.save(book);
                return "success";
            }).orElse("Book is not found");
        }).orElse("Author is not found");
    }

    public String addCategory(String titleBook, String nameCategory) {
        val byName = categoryRepository.findByName(nameCategory);
        return byName.map(category -> {
            val optBook = bookRepository.findByTitle(titleBook);
            return optBook.map(book -> {
                Set<Category> categories = book.getCategories();
                if (categories == null) {
                    categories = new HashSet<>();
                    book.setCategories(categories);
                }
                categories.add(category);
                bookRepository.save(book);

                return "success";
            }).orElse("Book is not found");
        }).orElse("Category is not found");
    }
}
